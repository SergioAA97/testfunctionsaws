// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');

// Set the region
AWS.config.update({ region: 'us-east-1' });

const tableName = process.env['DYNAMO_TABLE'];

const dynamoDB = new AWS.DynamoDB.DocumentClient();


exports.lambdaHandler = async (event) => {
    
    console.debug("Incoming event:", event);
    try {
        if(event.httpMethod !== 'GET') return {
            statusCode: 400,
            body: JSON.stringify(`Must be GET`)
        };
        
        // Handle
        if(event.httpMethod === 'GET') return await handleRequest(event);
        
        // Fallback
        return {
            statusCode: 200,
            body: JSON.stringify(`Powered by NexPlayer!`)
        };
    } catch (e) {
        return {
            statusCode: 500,
            body: JSON.stringify(e)
        };
    }
    
};

// Handler function for impression fetch
async function handleRequest(event){
    
    let queryParams = event.queryStringParameters;
    
    let response = {
            statusCode: 400,
            body: JSON.stringify('Bad Request. Must provide a query param "key"')
        };
    
    // Check query
    if(!queryParams){
        return response;
    }else if (!queryParams.hasOwnProperty('key')){
        return response;
    }
    
    const key = event.queryStringParameters.key;
    
    const year = new Date().getFullYear();
    
    const req = {
            TableName: tableName,
            Key: {
                key
            },
        }
        
    let res;
    
    try{
        res = await dynamoDB.get(req).promise();
    }catch(err){
        console.debug(err);
    }
    
    console.debug("Response from DynamoDB: ", res);
    if(!res) return {
        statusCode: 404,
        body: JSON.stringify('Not found')
    };

    let item = res.Item;
    
    
    // We process all years (in case in the future this is desired)
    let { years } = item;
    let total = [];
    // if(years) total = getTotalByMonth(years);
    
    if(!years) return {
        statusCode: 500,
        body: JSON.stringify('No impression data found or corrupted')
    };
    
    total = getTotalByMonth(years)
    
    console.debug("Result array: ", total);
    const thisYear = total[year];
    if(thisYear){
        response = {
            statusCode: 200,
            body: JSON.stringify(thisYear),
        };
    }else{
        response = {
            statusCode: 404,
            body: JSON.stringify('Year not found')
        };
    }
    
    return response;
}

//This function takes the years items from the impressions object
// and sums month by month with final shape:
// {
//     year: {
//         month: Number
//     }
// }

function getTotalByMonth(years){

    if(Object.keys(years).length === 0) return false;
    let result = {};
    let entries = Object.keys(years);
   
    for (var i = 0; i < entries.length; i++) {
        let year = entries[i];
        let t = years[year];
        let totalPerMonth = {};
        let m = Object.keys(t);
        let mCount = m.length; 
            
        if(mCount > 0){
           
            for (var j = 0; j < m.length; j++) {
                let month = m[j];
                if(t[month].total > 0){
                    totalPerMonth[month]= t[month].total;
                }
            }
            result[year] = totalPerMonth;
            console.debug("Result: ", result);
        }
    }
    
    return result;
}
