# NPM Tests

The events and test available for this function are:

| # | Command  | Event                                                           |
|---|----------|-----------------------------------------------------------------|
| 1 | npm test | Standard test event that fetches impressions for a license key  |